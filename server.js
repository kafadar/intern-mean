var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var jwt = require('jwt-simple');

var app = express();

var port = process.env.PORT || 3000;

var data = require('./config/database');
mongoose.connect(data.remote);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.set('view engine', 'jade');
app.set('views', __dirname + '/app/');
app.use(express.static(__dirname + '/app/'));

app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', 'http://localhost');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE');
    next();
});

require('./routes/routes')(app, express, jwt);
require('./routes/authRoutes')(app, express, jwt);

app.listen(port, function () {
    console.log('App is running on ' + port);
});