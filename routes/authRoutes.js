var Users = require('../models/User');
module.exports = function (app, express, jwt) {
    var authRouter = express.Router();

    authRouter.post('/register', function (req, res) {
        Users.findOne({
            email: req.body.email
        }, function (err, exist) {
            if (err) res.json({ status: "danger", message: "REGISTER ERROR"});
            if (exist) {
                res.json({
                    status: "warning",
                    message: "EMAIL TAKEN"
                });
            } else {
                var user = new Users();
                user.email = req.body.email;
                user.password = req.body.password;
                user.role = req.body.role;
                user.save(function (err) {
                    var payload = {
                        iss: req.hostname,
                        sub: user.id
                    }
                    var tkn = jwt.encode(payload, "**********");
                    res.json({
                        object: user.toJSON(),
                        token:  tkn,
                        status: "success",
                        message: "USER REGISTERED"
                    });
                });
            }
        });

    });

    app.use('/auth', authRouter);
}