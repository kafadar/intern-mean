var Interns = require('../models/Intern');
var Companies = require('../models/Company');
var Vacancies = require('../models/Vacancy');
module.exports = function (app, express, jwt) {
    //index route
    app.get('/', function (req, res) {
        res.render('index');
    });
    //api routes
    var internRouter = express.Router();
    internRouter.get('/interns', function (req, res) {
        Interns.find(function (err, interns) {
            if (err) res.send(err);
            res.json(interns);
        });
    }).get('/interns/:id', function (req, res) {
        Interns.findById(req.params.id, function (err, intern) {
            if (err) res.send(err);
            res.json(intern);
        });
    }).get('/interns/:id/subs', function (req, res) {
        Interns.findById(req.params.id, function (err, intern) {
            if (err) res.send(err);
            Companies.find({
                _id: {
                    $in: intern.subscribtions
                }
            }, function (err, comps) {
                if (err) res.send(err);
                res.json(comps);
            });
        });
    }).post('/interns', function (req, res) {
        var intern = new Interns();
        intern.user_id = req.body.user_id;
        intern.name.first = req.body.name.first;
        intern.name.last = req.body.name.last;
        intern.brief_info = req.body.brief_info;
        intern.email = req.body.email;
        intern.phone = req.body.phone;
        intern.certificates = req.body.certificates;
        intern.education = req.body.education;
        intern.skills = req.body.skills.split(",");
        intern.interests = req.body.interests.split(",");
        intern.subscribtions = req.body.subscribtions;
        intern.save(function (err) {
            if (err) res.send(err);
            res.json({
                status: "success",
                message: "INTERN ADDED"
            });
        });
    }).post('/interns/:id/subs/:cid', function (req, res) {
        Interns.update({
            _id: req.params.id
        }, {
            $addToSet: {
                subscribtions: req.params.cid
            }
        }, function (err, intern) {
            if (err) res.send(err);
            Companies.update({
                _id: req.params.cid
            }, {
                $addToSet: {
                    subscribers: req.params.id
                }
            }, function (err, company) {
                if (err) res.send(err);
                res.json({
                    status: "success",
                    message: "SUBSCRIBTION ADDED"
                });
            });
        });
    }).put('/interns/:id', function (req, res) {
        Interns.update({
            _id: req.params.id
        }, {
            $set: {
                name: req.body.name,
                brief_info: req.body.brief_info,
                email: req.body.email,
                phone: req.body.phone,
                certificates: req.body.certificates,
                education: req.body.education,
                skills: req.body.skills.split(","),
                interests: req.body.interests.split(","),
                subscribtions: req.body.subscribtions
            }
        }, function (err, intern) {
            if (err) res.send(err);
            res.json({
                status: "success",
                message: "INTERN UPDATED"
            });
        });
    }).delete('/interns/:id', function (req, res) {
        Interns.remove({
            _id: req.params.id
        }, function (err, intern) {
            if (err) res.send(err);
            res.json({
                status: 'success',
                message: 'INTERN DELETED'
            });
        });
    }).delete('/interns/:id/subs/:cid', function (req, res) {
        Interns.update({
            _id: req.params.id
        }, {
            $pull: {
                subscribtions: req.params.cid
            }
        }, function (err, intern) {
            if (err) res.send(err);
            res.json({
                status: "success",
                message: "SUBSCRIBTION DELETED"
            });
        });
    });
    var companyRouter = express.Router();
    companyRouter.get('/companies', function (req, res) {
        Companies.find(function (err, comps) {
            if (err) res.send(err);
            res.json(comps);
        });
    }).get('/companies/:cid', function (req, res) {
        Companies.findById(req.params.cid, function (err, comp) {
            if (err) res.send(err);
            res.json(comp);
        });
    }).get('/companies/:cid/vacancies', function (req, res) {
        Vacancies.find({
            company: req.params.cid
        }, function (err, vacans) {
            if (err) res.send(err);
            res.json(vacans);
        });
    }).get('/companies/:cid/subs', function (req, res) {
        Interns.find({
            subscribtions: req.params.cid
        }, function (err, interns) {
            if (err) res.send(err);
            res.json(interns);
        });
    }).post('/companies', function (req, res) {
        var company = new Companies();
        company.user_id = req.body.user_id;
        company.title = req.body.title;
        company.website = req.body.website;
        company.description = req.body.description;
        company.save(function (err) {
            if (err) res.send(err);
            res.json({
                status: "success",
                message: "COMPANY ADDED"
            });
        });
    }).put('/companies/:cid ', function (req, res) {
        Companies.update({
            _id: req.params.cid
        }, {
            $set: {
                title: req.body.title,
                website: req.body.website,
                description: req.body.description
            }
        }, function (err, company) {
            if (err) res.send(err);
            res.json({
                status: "success",
                message: "COMPANY UPDATED"
            });
        });
    }).delete('/companies/:cid', function (req, res) {
        Companies.remove({
            _id: req.params.cid
        }, function (err, company) {
            if (err) res.send(err);
            res.json({
                status: "success",
                message: "COMPANY DELETED"
            });
        });
    });
    var vacancyRouter = express.Router();
    vacancyRouter.get('/vacancies', function (req, res) {
        Vacancies.find(function (err, vacans) {
            if (err) res.send(err);
            res.json(vacans);
        });
    }).get('/vacancies/:vid', function (req, res) {
        Vacancies.findById(req.params.vid, function (err, vacan) {
            if (err) res.send(err);
            res.json(vacan);
        });
    }).get('/vacancies/:vid/applicants', function (req, res) {
        Vacancies.findById(req.params.vid, function (err, vacan) {
            if (err) res.send(err);
            Interns.find({
                _id: {
                    $in: vacan.applicants
                }
            }, function (err, interns) {
                if (err) res.send(err);
                res.json(interns);
            });
        });
    }).get('/vacancies/:vid/com', function (req, res) {
        Vacancies.findById(req.params.vid, function (err, vacan) {
            if (err) res.send(err);
            Companies.findById(vacan.company, function (err, company) {
                if (err) res.send(err);
                res.json(company);
            });
        });
    }).post('/vacancies', function (req, res) {
        var vacan = new Vacancies();
        vacan.user_id = req.body.user_id;
        vacan.title = req.body.title;
        vacan.company = req.body.company;
        vacan.internship = req.body.internship;
        vacan.fields = req.body.fields.split(",");
        vacan.requires = req.body.requires.split(",");
        vacan.description = req.body.description;
        vacan.applicants = req.body.applicants;
        vacan.save(function (err) {
            if (err) res.send(err);
            res.json({
                status: "success",
                message: "VACANCY ADDED"
            });
        });
    }).post('/vacancies/:vid/applicants/:id', function (req, res) {
        Vacancies.update({
            _id: req.params.vid
        }, {
            $addToSet: {
                applicants: req.params.id
            }
        }, function (err, vacan) {
            if (err) res.send(err);
            res.json({
                status: "success",
                message: "APPLICANT ADDED"
            });
        });
    }).put('/vacancies/:vid', function (req, res) {
        Vacancies.update({
            _id: req.params.vid
        }, {
            $set: {
                title: req.body.title,
                company: req.body.company,
                internship: req.body.internship,
                fields: req.body.fields.split(","),
                requires: req.body.requires.split(","),
                description: req.body.description
            }
        }, function (err, vacan) {
            if (err) res.send(err);
            res.json({
                status: "success",
                message: "VACANCY UPDATED"
            });
        });
    }).delete('/vacancies/:vid/applicants/:id', function (req, res) {
        Vacancies.update({
            _id: req.params.vid
        }, {
            $pull: {
                applicants: req.params.id
            }
        }, function (err, vacan) {
            if (err) res.send(err);
            res.json({
                status: "success",
                message: "APPLICANT DELETED"
            });
        });
    }).delete('/vacancies/:vid', function (req, res) {
        Vacancies.remove({
            _id: req.params.vid
        }, function (err, vacan) {
            if (err) res.send(err);
            res.json({
                status: "success",
                message: "VACANCY DELETED"
            });
        });
        ;
    });
    app.use('/api', authenticated, internRouter);
    app.use('/api', authenticated, companyRouter);
    app.use('/api', authenticated, vacancyRouter);

    function authenticated(req, res, next) {
        if (!req.headers.authorization) {
            return res.status(401).send({
                status: "warning",
                message: "NOT AUTHORIZED"
            });
        }
        var token = req.headers.authorization.split(' ')[1];
        var payload = jwt.decode(token, "fD0NHyBQNs");
        console.log(payload);
        if (!payload.sub) {
            return res.status(401).send({
                status: "warning",
                message: "AUTHENTICATION FAILED"
            });
        }
        next();
    }
}