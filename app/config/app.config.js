'use strict';

angular.module('internDB').config(function ($urlRouterProvider, $stateProvider, $httpProvider) {
    $urlRouterProvider.otherwise('/');
    $stateProvider.state('home', {
        url: '/',
        templateUrl: '../partials/welcome.html'
    }).state('register', {
        url: '/register/:role',
        templateUrl: '../partials/register/register.html',
        controller: 'RegisterCtrl'
    }).state('internList', {
        url: '/internList',
        templateUrl: '../partials/intern/internList.html',
        controller: 'InternListCtrl'
    }).state('internPost', {
        url: '/internPost',
        templateUrl: '../partials/intern/internPost.html',
        controller: 'InternPostCtrl'
    }).state('internDetail', {
        url: '/internDetail/:id',
        templateUrl: '../partials/intern/internDetail.html',
        controller: 'InternDetailCtrl'
    }).state('vacancyList', {
        url: '/vacancyList',
        templateUrl: '../partials/vacancy/vacancyList.html',
        controller: 'VacancyListCtrl'
    }).state('vacancyPost', {
        url: '/vacancyPost',
        templateUrl: '../partials/vacancy/vacancyPost.html',
        controller: 'VacancyPostCtrl'
    }).state('vacancyDetail', {
        url: '/vacancyDetail/:id',
        templateUrl: '../partials/vacancy/vacancyDetail.html',
        controller: 'VacancyDetailCtrl'
    }).state('companyList', {
        url: '/companyList',
        templateUrl: '../partials/company/companyList.html',
        controller: 'CompanyListCtrl'
    }).state('companyPost', {
        url: '/companyPost',
        templateUrl: '../partials/company/companyPost.html',
        controller: 'CompanyPostCtrl'
    }).state('companyDetail', {
        url: '/companyDetail/:id',
        templateUrl: '../partials/company/companyDetail.html',
        controller: 'CompanyDetailCtrl'
    });

    $httpProvider.interceptors.push('authIntercept');
})
//constant
    .constant('API', 'http://localhost:3000/api')
//constant
    .constant('AUTH_URL', 'http://localhost:3000/auth')
//config
    .config(function (localStorageServiceProvider) {
        localStorageServiceProvider.setPrefix('intern');
    });
/*
 'https://internapi.herokuapp.com/api'
 'http://localhost:3000/api'
 */