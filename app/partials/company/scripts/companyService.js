angular.module('internDB').factory('companyService', function ($http, API) {
    var baseUrl = API + '/companies/';
    return {
        getCompanyList: function () {
            return $http.get(baseUrl);
        },
        getCompany: function (id) {
            return $http.get(baseUrl + id);
        },
        getCompanyVacancies: function (id) {
            return $http.get(baseUrl + id + '/vacancies');
        },
        getCompanySubs: function (id) {
            return $http.get(baseUrl + id + '/subs');
        },
        postCompany: function (data) {
            return $http.post(baseUrl, data);
        },
        deleteCompany: function (id) {
            return $http.delete(baseUrl + id);
        }
    };
});