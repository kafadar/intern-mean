angular.module('internDB').controller('CompanyDetailCtrl', function ($scope, $state, $stateParams, companyService, alertService) {
    $scope.pagetitle = 'Single company';
    companyService.getCompany($stateParams.id).success(function (data) {
        $scope.company = data;
    }).error(function (data) {
        alertService(data.status, data.message);
    });
    companyService.getCompanyVacancies($stateParams.id).success(function (data) {
        $scope.vacancies = data;
    });
    companyService.getCompanySubs($stateParams.id).success(function (data) {
        $scope.subscribers = data;
    });
    $scope.delete = function () {
        companyService.deleteCompany($scope.company._id).success(function (data) {
            $state.go('companyList');
        });
    }
});