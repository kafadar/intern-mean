angular.module('internDB').controller('CompanyPostCtrl', function ($scope, $state, companyService, alertService) {
    $scope.pagetitle = 'Post Company';
    $scope.postCompany = function () {
        var company = {
            title: $scope.title,
            website: $scope.website,
            description: $scope.description
        };
        companyService.postCompany(company).success(function (data) {
            alertService(data.status, data.message);
            $state.go('companyList');
        });
    }
});