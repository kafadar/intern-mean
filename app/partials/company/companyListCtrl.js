angular.module('internDB').controller('CompanyListCtrl', function ($scope, companyService, alertService) {
    $scope.pagetitle = 'Companies';
    companyService.getCompanyList().success(function (data) {
        $scope.companies = data;
    }).error(function (data) {
        alertService(data.status, data.message);
    });
});