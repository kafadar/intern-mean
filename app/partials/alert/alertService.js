angular.module('internDB').service('alertService', function ($rootScope, $timeout) {
    return function (type, message, timeout) {
        var alertTimeout;
        $rootScope.alert = {
            hasBeenShown: true,
            show: true,
            type: type,
            message: message
        };
        $timeout.cancel(alertTimeout);
        alertTimeout = $timeout(function () {
            $rootScope.alert.show = false;
        }, timeout || 2500);
    }
});