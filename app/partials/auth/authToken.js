angular.module('internDB').factory('authToken', function (localStorageService) {
    var cachedToken;
    var key = 'user_token';
    return {
        setToken: function (token) {
            cachedToken = token;
            localStorageService.set(key, token);
        },
        getToken: function () {
            if (!cachedToken)
                cachedToken = localStorageService.get(key);
            return cachedToken;
        },
        isAuthenticated: function () {
            return !!this.getToken();
        },
        removeToken: function () {
            cachedToken = null;
            localStorageService.clearAll();
        }
    }
})