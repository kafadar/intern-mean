angular.module('internDB').controller('AuthCtrl', function ($scope, $state, authToken, alertService) {
    $scope.isAuth = function () {
        return authToken.isAuthenticated();
    }
    $scope.logout = function () {
        authToken.removeToken();
        alertService("success", "See you soon");
        $state.go('home');
    }
    $scope.showToken = function () {
        return authToken.getToken();
    }
});