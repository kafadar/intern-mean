angular.module('internDB').factory('vacancyService', function ($http, API) {
    var baseUrl = API + '/vacancies/';
    return {
        getVacancyList: function () {
            return $http.get(baseUrl);
        },
        getVacancy: function (id) {
            return $http.get(baseUrl + id);
        },
        getVacancyCompany: function (id) {
            return $http.get(baseUrl + id + '/com');
        },
        getVacancyApplicants: function (id) {
            return $http.get(baseUrl + id + '/applicants');
        },
        postVacancy: function (data) {
            return $http.post(baseUrl, data);
        },
        postApplicant: function (vid, id) {
            return $http.post(baseUrl + vid + '/applicants/' + id);
        },
        deleteVacancy: function (id) {
            return $http.delete(baseUrl + id);
        },
        deleteApplicant: function (vid, id) {
            return $http.delete(baseUrl + vid + '/applicants/' + id);
        }
    };
});