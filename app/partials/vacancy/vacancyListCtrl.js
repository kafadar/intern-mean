angular.module('internDB').controller('VacancyListCtrl', function ($scope, vacancyService, alertService) {
    $scope.pagetitle = 'Vacancies';
    vacancyService.getVacancyList().success(function (data) {
        $scope.vacancies = data;
    }).error(function (data) {
        alertService(data.status, data.message);
    });
});