angular.module('internDB').controller('VacancyDetailCtrl', function ($scope, $state, $stateParams, vacancyService, authService, alertService) {
    $scope.pagetitle = 'Single vacancy';
    vacancyService.getVacancy($stateParams.id).success(function (data) {
        $scope.vacancy = data;
    }).error(function (data) {
        alertService(data.status, data.message);
    });
    vacancyService.getVacancyApplicants($stateParams.id).success(function (data) {
        $scope.applicants = data;
    });
    vacancyService.getVacancyCompany($stateParams.id).success(function (data) {
        $scope.company = data;
    });
    //scope functions
    $scope.delete = function () {
        vacancyService.deleteVacancy($scope.vacancy._id).success(function (data) {
            alertService(data.status, data.message);
            $state.go('vacancyList');
        });
    }
    $scope.apply = function () {
        /*
            TODO Implementation of apply function
         */
    }
});