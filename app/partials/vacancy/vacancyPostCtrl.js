angular.module('internDB').controller('VacancyPostCtrl', function ($scope, $state, vacancyService, alertService) {
    $scope.pagetitle = 'Post Vacancy';
    $scope.internship = false;
    $scope.postVacancy = function () {
        var vacancy = {
            title: $scope.title,
            description: $scope.description,
            company: authService.getCurrentCompany(),
            requires: $scope.requires,
            fields: $scope.fields,
            internship: $scope.internship
        };
        vacancyService.postVacancy(vacancy).success(function (data) {
            alertService(data.status, data.message);
            $state.go('vacancyList');
        });
    }
});