angular.module('internDB').controller('RegisterCtrl', function ($scope, $stateParams, registerService, alertService, authToken) {
    $scope.submit = function () {
        if (!authToken.isAuthenticated()) {
            var user = {
                email: $scope.email,
                password: $scope.password,
                role: $stateParams.role
            }
            registerService.registerUser(user).success(function (data) {
                alertService(data.status, data.message);
                authToken.setToken(data.token);
            }).error(function (data) {
                alertService(data.status, data.message);
            });
        } else {
            alertService("warning", "You are already authenticated");
        }

    }
});