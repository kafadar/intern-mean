angular.module('internDB').factory('registerService', function ($http, AUTH_URL) {
    var baseUrl = AUTH_URL + '/register';
    return {
        registerUser: function (data) {
            return $http.post(baseUrl, data);
        }
    };
});