angular.module('internDB').controller('InternDetailCtrl', function ($scope, $state, $stateParams, internService, authToken, alertService) {
    $scope.pagetitle = 'Single intern';
    internService.getIntern($stateParams.id).success(function (data) {
        $scope.intern = data;
    }).error(function (data) {
        alertService(data.status, data.message);
    });
    internService.getInternSubs($stateParams.id).success(function (data) {
        $scope.subs = data;
    });
    $scope.delete = function () {
        internService.deleteIntern($scope.intern._id).success(function (data) {
            alertService(data.status, data.message);
            $state.go('internList');
        });
    }
});