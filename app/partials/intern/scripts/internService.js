angular.module('internDB').factory('internService', function ($http, API) {
    var baseUrl = API + '/interns/';
    return {
        getInternList: function () {
            return $http.get(baseUrl);
        },
        getIntern: function (id) {
            return $http.get(baseUrl + id);
        },
        getInternSubs: function (id) {
            return $http.get(baseUrl + id + '/subs');
        },
        postIntern: function (data) {
            return $http.post(baseUrl, data);
        },
        postSubs: function (id, cid) {
            return $http.post(baseUrl + id + '/subs/' + cid);
        },
        deleteIntern: function (id) {
            return $http.delete(baseUrl + id);
        },
        deleteSub: function (id, cid) {
            return $http.delete(baseUrl + id + '/subs/' + cid);
        }
    };
});