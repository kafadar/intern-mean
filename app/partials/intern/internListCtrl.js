angular.module('internDB').controller('InternListCtrl', function ($scope, internService, alertService) {
    $scope.pagetitle = 'Interns';
    internService.getInternList().success(function (data) {
        $scope.interns = data;
    }).error(function (data) {
        alertService(data.status, data.message);
    });
});