angular.module('internDB').controller('InternPostCtrl', function ($scope, $state, internService, alertService) {
    $scope.pagetitle = 'Post Intern';
    $scope.postIntern = function () {
        var intern = {
            name: {
                first: $scope.first,
                last: $scope.last
            },
            email: $scope.email,
            phone: $scope.phone,
            brief_info: $scope.brief_info,
            education: {
                school: $scope.school,
                course: $scope.course,
                degree: $scope.degree
            },
            skills: $scope.skills,
            interests: $scope.interests
        };
        internService.postIntern(intern).success(function (data) {
            alertService(data.status, data.message);
            $state.go('internList');
        });
    }
});