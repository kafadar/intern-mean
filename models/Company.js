var mongoose = require('mongoose');
var companySchema = mongoose.Schema({
    user_id: {
        type: String,
        required: true
    },
    title: String,
    website: String,
    description: String,
    subscribers: [String],
    created: {
        type: Date,
        default: Date.now
    }
});
module.exports = mongoose.model('CompanyProfile', companySchema);