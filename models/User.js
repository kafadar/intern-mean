var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

var userSchema = mongoose.Schema({
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: String,
    role: {
        type: String,
        default: "intern"
    },
    dates: {
        registered: {
            type: Date,
            default: Date.now
        },
        last_login: {
            type: Date,
            default: Date.now
        }
    },
    approved: {
        type: Boolean,
        default: false
    }
});

userSchema.methods.toJSON = function () {
    var user = this.toObject();
    delete user.password;
    delete user.approved;
    delete user.dates;
    return user;
}

module.exports = mongoose.model('User', userSchema);

userSchema.pre('save', function (next) {
    var user = this;

    if (!user.isModified('password')) return next();

    bcrypt.genSalt(10, function (err, salt) {
        if (err) return next(err);

        bcrypt.hash(user.password, salt, null, function (err, hash) {
            if (err) return next(err);
            user.password = hash;
            next();
        });
    });
});