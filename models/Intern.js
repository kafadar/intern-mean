var mongoose = require('mongoose');
var internSchema = mongoose.Schema({
    user_id: {
        type: String,
        required: true
    },
    name: {
        first: String,
        last: String
    },
    brief_info: String,
    email: String,
    phone: String,
    education: [{
        school: String,
        course: String,
        degree: String
    }],
    certificates: [{
        title: String,
        acquired: String
    }],
    skills: [String],
    interests: [String],
    subscribtions: [String],
    approved: {
        type: Boolean,
        default: false
    },
    created: {
        type: Date,
        default: Date.now
    }
});
module.exports = mongoose.model('InternProfile', internSchema);