var mongoose = require('mongoose');
var vacancySchema = mongoose.Schema({
    user_id: {
        type: String,
        required: true
    },
    title: String,
    company: String,
    internship: Boolean,
    fields: [String],
    requires: [String],
    description: String,
    applicants: [String],
    created: {
        type: Date,
        default: Date.now
    }
});
module.exports = mongoose.model('Vacancy', vacancySchema);